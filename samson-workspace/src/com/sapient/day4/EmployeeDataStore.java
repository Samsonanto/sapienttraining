package com.sapient.day4;

import java.util.ArrayList;

import java.util.List;

public class EmployeeDataStore {

	ArrayList<Employee> eList = new ArrayList<Employee>();
	
	
	void addEmp(Employee e) {
		eList.add(e);
	}
	
	Employee getEmp(int i) {
		return eList.get(i);
	}
	
	ArrayList<Employee> getAllEmp() {
		return eList;
	}
	
	boolean removeEmp(Employee e) {
		
		return eList.remove(e);
		
	}
	
	
}
