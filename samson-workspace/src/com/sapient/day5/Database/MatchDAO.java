package com.sapient.day5.Database;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.util.ArrayList;
	import java.util.List;

	public class MatchDAO {
			
		public static List<MatchBean> getPlayers() throws Exception{
			
			Connection con = DBConnect.getConnection();
			
			List<MatchBean> l = new ArrayList<MatchBean>();
			
			PreparedStatement pstmt = con.prepareStatement("select * from mdesp");
			
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()) {
				
				l.add(new MatchBean(rs.getString(2),rs.getInt(1)));
			}
			return l; 
		}
		
		public static void insertPlayer(int mid , String mname) throws Exception {
			
			Connection con = DBConnect.getConnection();
			
			PreparedStatement pstm = con.prepareStatement("insert into player (mid,mnane) values (?,? )");
			pstm.setInt(1, mid);
			pstm.setString(2, mname);
			
			pstm.execute();
		}
		
		public static void deleteByIdFromMatch(int id) throws Exception {
			
			Connection con = DBConnect.getConnection();
			
			PreparedStatement p = con.prepareStatement("delete from player where mid = ? ");
			p.setInt(1,id);
			p.execute();
			
		}
	}
