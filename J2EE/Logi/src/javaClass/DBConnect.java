package javaClass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author sampaul
 *
 */
public class DBConnect {

	private static Connection con=null;
	
	static Connection getConnection() throws SQLException, ClassNotFoundException   {
		
		if(con == null) {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:xe","system","samson");
		}
		return con;
	}
}