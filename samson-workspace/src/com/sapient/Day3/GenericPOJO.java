package com.sapient.Day3;

public class GenericPOJO <T> {
	
	T data;
	
	public GenericPOJO(T data) {
		this.data = data;
	}
	
	void setData(T data) {
		this.data = data;
	}
	
	T getData(){
		return data;
	}

}
