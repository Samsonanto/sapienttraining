/*
 *
 * Program to convert a given amount to words
 * Here we a to take
 *
 *
 * */


package com.sapient.day2;

public class AmountToWord {
    public static String getWords(long amt) {
        String words = "";

        String[] unit = {"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirtine", "Fortine", "Fiftine", "Sixtine", "Seventine", "Eightine", "Nintine"};
        String[] tens = {"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninty"};
        String[] vunit = {"crores", "Lakes", "Thousands", "Hundred"};
        long[] nunit = {10000000L, 100000L, 1000L, 100L, 1L};

        for (int i = 0; i < nunit.length; i++) {
            int n = (int) (amt / nunit[i]);
            amt = amt % nunit[i];
            if (n > 0) {
                if (n > 19) {
                    words += tens[n / 10] + " " + unit[n % 10] + " " + vunit[i] + " ";

                } else {
                    words += unit[n] + vunit[i];
                }
            }
        }

        return words;
    }
}
