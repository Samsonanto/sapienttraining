package com.sapient.Day3.MVCDemo;

import com.sapient.util.Read;

import java.util.Scanner;
import java.util.concurrent.locks.ReadWriteLock;

public class View {


    static void getInput(Number a){
        System.out.println("Enter a number ");

        a.setNum(Read.in.nextInt());
    }

    static void Display(Number n){

        System.out.println("The result is : "+n.getNum());
    }
}
