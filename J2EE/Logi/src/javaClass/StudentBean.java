package javaClass;

import java.util.Map;

public class StudentBean extends PeopleBean {

	private Map<String,Float> marks;
	
	public StudentBean(int rollNumber, String name,int mode, Map marks) {
		super(rollNumber,name,mode);
		this.marks = marks;
	}
	public StudentBean() {
		super();
	}
	
	public Map<String,Float> getMarks() {
		return marks;
	}
	public void setMarks(Map<String,Float> marks) {
		this.marks = marks;
	}
	
}
