package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javaClass.PeopleDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out = response.getWriter();
		
		out.print(" <form action=\"Login\" method=\"post\" >\r\n" + 
					"	User ID: <input type=\"text\" name=\"id\"><br>\r\n" + 
					" 	Password: <input type=\"password\" name=\"pass\"><br>\r\n" + 
					"  <input type=\"submit\" value=\"Login\">\r\n" + 
				  "</form> ");
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.setAttribute("id",request.getParameter("id"));
		
		int id  = Integer.parseInt(request.getParameter("id"));
		String pass = request.getParameter("pass");
		
		try {
			if(PeopleDAO.validate(id, pass)) {
				int access = PeopleDAO.getAccessRights(id);
				if(access == 1) { 
					request.getRequestDispatcher("/Admin").include(request, response);  
					
				}else {
					request.getRequestDispatcher("/Student").forward(request, response);
				}
			}else {
				response.getWriter().print("<h1> Invalid User </h1>");
			}
			
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

}
