package com.sapient.day4;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import com.sapient.util.Read;

public class EmployeeDAO {
	
	EmployeeDataStore db = new EmployeeDataStore();
	
	public EmployeeDAO() throws Exception {
		// TODO Auto-generated constructor stub
		
		Charset charset = Charset.forName("US-ASCII");
		Path path = FileSystems.getDefault().getPath("Test3.csv");
		
		BufferedReader br = Files.newBufferedReader(path,charset);

		String line;
		
		while((line = br.readLine() ) != null) {
			
			String[] res = line.split(",");
			
			db.addEmp(new Employee(res[0], res[1], Integer.parseInt(res[2]),Long.parseLong(res[3])));
			
			
		}
		
		br.close();		
		
	}

	public void display() {
		// TODO Auto-generated method stub
		for(Employee e : db.getAllEmp()) {
			System.out.println(e.toString());
		}
	}

	public EmployeeDataStore getDb() {
		// TODO Auto-generated method stub
		return db;
	}
	
	
	
	

}
