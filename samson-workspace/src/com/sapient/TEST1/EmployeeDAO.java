package com.sapient.TEST1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {
	
	
	
	public  List<EmployeeBean> readData() throws Exception{
		
		List<EmployeeBean> empList = new ArrayList<EmployeeBean>();
		
		File empCSV = new File("C:\\Users\\sampaul\\Desktop\\sapient-training\\samson-workspace\\src\\com\\sapient\\TEST1\\Data.csv");
		BufferedReader br = new BufferedReader(new FileReader(empCSV));
		
		String line = null;
		
		while( (line = br.readLine()) != null ) {
			
			String[] tmp = line.split(",");
			
			
			if(tmp.length != 3) {
				throw new Exception("Invalid CSV File");
			}
			empList.add( new EmployeeBean( Float.parseFloat(tmp[2]),Long.parseLong(tmp[0]),tmp[1] ) );
			
		}
	
		return empList;		
		
	}
	
	public float getTotSal(List<EmployeeBean> l) {
		
		
		double sum = l.stream().mapToDouble( e ->e.getSal()).sum();	
		
		return (float)sum;
	}
	
	public int intGetCount(List<EmployeeBean> l,int sal) {
		
		return (int) l.stream().filter((e) -> { return e.getSal() > sal; }).count();
		
	}
	
	public EmployeeBean getEmployee(int id) throws Exception {
		
		for(EmployeeBean e : readData()) {
			
			if(e.getId() == id) {
				return e;
			}
		}
		
		return null;
	}
}
