package com.sapient.Day3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.sapient.util.Read;

public class EmployeeMain {
	
	static void display(List<Employee> l) {
		for(Employee e : l)
			System.out.println(e.address + " : " + " : " + e.name + " : " + e.eid);
	}
	
	

	public static void main(String[] args)
	{
		EmployeeDataStore eds = new EmployeeDataStore();
		
		
		eds.addEmployee(new Employee("Z1","Addr1"));
		eds.addEmployee(new Employee("C2", "zaddr2"));
		eds.addEmployee(new Employee("S3", "addr3"));
		
		
		System.out.println("Enter the eid to be searched ");
		Long l1 = Read.in.nextLong();
		
		Employee res = eds.getDetails(l1);
		
		System.out.println("Employee details name : "+res.getName()+" Address : "+res.getAddress());
		
		System.out.println("Sorting by name");
		
		List<Employee> l = eds.getList();
		
		Collections.sort(l,new AddressComparator());
		System.out.println("Sorted by address");
		display(l);
		
		
		Collections.sort(l,new NameComparator());
		System.out.println("Sorted by name");
		display(l);
		
		
	}
	
}

