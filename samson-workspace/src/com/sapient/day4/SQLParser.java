package com.sapient.day4;

import java.util.ArrayList;

import com.sapient.util.Read;

public class SQLParser {
	
	public static void main(String[] args) throws Exception {
		
		EmployeeDAO eDAO = new EmployeeDAO();
		
		EmployeeDataStore db = eDAO.getDb();
		
		ArrayList<Employee> l = db.getAllEmp();
		
		
		System.out.println("Enter the SQL");
		
		String query = Read.in.nextLine();
		
		String[] part = query.split(" ");
		
		int[] tp = {0,0,0,0};
		
		if(!part[0].equalsIgnoreCase("select"))
		{
			System.out.println("Invalid query");
		}else {
		
			int i=0;
			for(String s : part[1].split(",") ) {
			
				if(s.equals("*")) {
					eDAO.display();
					break;
				}
				else if(s.equalsIgnoreCase("name")) {
					 tp[0] = 1;
				}else if(s.equalsIgnoreCase("city")) {
					tp[1] = 1;
				}else if(s.equalsIgnoreCase("age")) {
					tp[2] = 1;
				}else if(s.equalsIgnoreCase("sal")) {
					tp[3] = 1;
				}
			}
		}
		
		for(Employee e : l) {
			if(tp[0] == 1) {
				System.out.print(e.getName()+" :");
			}if(tp[1] == 1) {
				System.out.print(e.getCity()+" :");
			}if(tp[2] == 1) {
				System.out.print(e.getAge()+" :");
			}if(tp[3] == 1) {
				System.out.println(e.getSal());
			}
		}
		
		
	}
	
}
