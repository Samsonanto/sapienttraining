package com.sapient.day4.CardGame;

import java.util.ArrayList;
import java.util.Collections;

public class Main {

	
	static int cur =1;

	
	static String display(int i) {
		return "";
	}
	
	
	public static void main(String[] args) throws Exception {
		
		
		ArrayList<Integer> al = new ArrayList<>();
		
		
		for(int i=1;i<=30;i++) {
			
			al.add(i);
			
		}
		
		Collections.shuffle(al);
		
		
		
		
		Runnable r1  =  () ->{
			String ip;
			do {
				 ip = display(1); 
				 while(cur != 1  && !ip.equalsIgnoreCase("exit")) {};
			}while( ! ip.equalsIgnoreCase("exit")  );
			
			System.out.println("Thread 1 Dead");
		};
		
		
		Runnable r2  =  () ->{
			String ip;
			do {
				ip = display(2); 
				while(cur != 2 && !ip.equalsIgnoreCase("exit")){};
			}while( ! ip.equalsIgnoreCase("exit")  );
			
			System.out.println("Thread 2 Dead");
		};
		
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		System.out.println("Main Thread Dead");
		
		
	}
	
	
	
}
