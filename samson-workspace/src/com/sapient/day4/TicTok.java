package com.sapient.day4;

import com.sapient.util.Read;

public class TicTok {
	
	static int cur =1;
	
	public static synchronized String display(int n)
	{
			
			cur = cur == 1 ? 2:1;
			System.out.println("In Thread "+n );
			return Read.in.nextLine();
		
		
	}
	public static void main(String[] args) throws InterruptedException {
		
		Runnable r1  =  () ->{
			String ip;
			do {
				 ip = display(1); 
				 while(cur != 1  && !ip.equalsIgnoreCase("exit")) {};
			}while( ! ip.equalsIgnoreCase("exit")  );
			
			System.out.println("Thread 1 Dead");
		};
		
		
		Runnable r2  =  () ->{
			String ip;
			do {
				ip = display(2); 
				while(cur != 2 && !ip.equalsIgnoreCase("exit")){};
			}while( ! ip.equalsIgnoreCase("exit")  );
			
			System.out.println("Thread 2 Dead");
		};
		
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		
		t1.start();
		t2.start();
		
		t1.join();
		t2.join();
		
		System.out.println("Main Thread Dead");
		
	}
	
	

}
