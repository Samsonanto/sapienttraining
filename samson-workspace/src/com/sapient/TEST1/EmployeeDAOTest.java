package com.sapient.TEST1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;

class EmployeeDAOTest {

	EmployeeDAO eDAO;
	List<EmployeeBean> l;
	@Before
	void init() {
		
		try {
		eDAO = new EmployeeDAO();
		l = eDAO.readData();
		}catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
		}
	}
	
	@Test
	void getTotSalTest() {
		
		try {
			float sum = eDAO.getTotSal(l);
			
			Assert.assertEquals(657000f,sum,0.000001f );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	void intGetCountTest() {
		
		try {
			
			int sum = eDAO.intGetCount(l,0);
			
			Assert.assertEquals(12,sum );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	void getEmployeeTest() {
		
		try {
			
			EmployeeBean e = eDAO.getEmployee(0);
			
			boolean res = true;
			
			if(!e.getName().equalsIgnoreCase("Samson")) {
				res = false;
			}
			
			if(e.getSal() != 50000f)
				res = false;
			
			Assert.assertEquals(true,res );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
