package com.sapient.student.dao;

import java.sql.*;

public class DBConnect {
static Connection con = null;
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		if(con==null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","samson"); 
			con.setAutoCommit(true); 
		}
		return con;
		
	}

}
