#include<stdio.h>


int add(int a,int b){
    return a+b;
}

int sub(int a,int b){
    return a-b;
}

int mul(int a,int b){
    return a*b;
}

int div(int a,int b){
    return a/b;
}

int main(int argc, char *argv[]){

    int (*res[4])(int,int) ;

    res[0] = add;
    res[1] = sub;
    res[2] = mul;
    res[3] = div;

    int result = 0;


    int a = (int)(argv[2][0] - '0');
    int b = (int)(argv[3][0] - '0');
    int index = (int)(argv[1][0] - '0');



    result = res[index-1](a,b);
    
    printf("%d \n",result);

    return 0;
}