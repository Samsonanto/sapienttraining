package com.sapient.day2;

class IntArray {
    private int[] arr;

    //    Default Constructor with size 10
    IntArray(){
        this.arr = new int[10];
    }

//    Parameterized Constructor with parameter size

    IntArray(int size) {
        this.arr = new int[size];
    }

//    Copy constructor

    IntArray(IntArray ob){
        this.arr = ob.getArr();
    }

    //    adopting constructor
    IntArray(int[] a) {
        this.arr = a;
    }

//    It will take n element as input and assign to the array

    void setArr(){


        System.out.println("Enter " + arr.length + " Element");

        for(int i=0;i< arr.length;i++)
        {
            arr[i] = Input.sc.nextInt();
        }
    }

//    returns the array

    int[] getArr(){
        return this.arr;
    }


    //    Display the array
    void display(){
        System.out.println("Display");
        for (int value : arr) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

//    return the average of the array elements

    int avg(){
        int sum = 0;

        for(int a: arr){
            sum+=a;
        }
        return sum/arr.length;
    }

    //    Sorts the array
    void sort(){
        for(int i = 0; i<arr.length; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if(arr[j] > arr[j+1] ){
                    arr[j] +=arr[j+1];
                    arr[j+1] = arr[j] - arr[j+1];
                    arr[j] = arr[j] - arr[j+1];
                }
            }
        }
    }

    //    search for an element in the array
    boolean search(int n){
        for(int i : arr)
            if(i == n)
                return true;
        return false;
    }
}
