package com.sapient.Day3.MVCDemo;

public class Number {

    private int a;

    Number(int a){
        this.a = a;
    }
    Number(){
        a = 0;
    }

    public int getNum() {
        return a;
    }

    public void setNum(int a) {
        this.a = a;
    }
}
