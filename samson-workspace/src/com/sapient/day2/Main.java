package com.sapient.day2;

public class Main {

    public static void main(String[] args) {


        IntArray ia = new IntArray(5);
        ia.setArr();
        ia.display();
        System.out.println("Average : " + ia.avg());
        System.out.println("Search result : " + ia.search(5));
        ia.sort();
        ia.display();

        Matrix m = new Matrix();
        m.setMatrix();
        Matrix m2 = new Matrix();
        m2.setMatrix();

        int[][] res = m.mul(m2.getMatrix());
        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res.length; j++) {
                System.out.print(res[i][j] + " ");
            }
            System.out.println();
        }
    }
}
