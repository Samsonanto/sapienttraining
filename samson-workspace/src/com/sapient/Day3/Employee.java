package com.sapient.Day3;

public class Employee {
	
	long eid ;
	String name;
	String address;
	static long counter = 0 ;
	
	public Employee(String name,String addr) {
		// TODO Auto-generated constructor stub
		
		this.eid = counter++;
		this.name = name;
		this.address = addr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getEid() {
		return eid;
	}
}
