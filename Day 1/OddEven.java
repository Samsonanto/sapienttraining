import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

class OddEven{

    static Logger logger = Logger.getLogger(OddEven.class);
    public static void main(String[] args){

        List<Integer> list = new ArrayList<>();
        for(String s : args)
     
        list.add(Integer.parseInt(s));


        Predicate<Integer> p = (x) -> x%2 == 0; 
/*
Using Streams
        int sumEven = list.stream().filter(x -> x%2 == 0).reduce(0, (a,b)-> a+b );

        int sumOdd = list.stream().filter(x -> x%2 != 0).reduce(0, (a,b)-> a+b );
*/
        logger.info("Result of Even no. " + sumEven + "Result of odd no. "+ sumOdd);
    }
}