package com.sapient.day5.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PlayerDAO {
		
	public static List<PlayerBean> selectAllPlayers() throws Exception{
		
		Connection con = DBConnect.getConnection();
		
		List<PlayerBean> l = new ArrayList<PlayerBean>();
		
		PreparedStatement pstmt = con.prepareStatement("select * from player");
		
		ResultSet rs = pstmt.executeQuery();
		
		while(rs.next()) {
			
			l.add(new PlayerBean(rs.getInt(1),rs.getInt(4),rs.getString(2),rs.getString(3)));
		}
		return l; 
	}
	
	public static void insertPlayer(int pid , String fname,String lname,int jno) throws Exception {
		
		Connection con = DBConnect.getConnection();
		
		PreparedStatement pstm = con.prepareStatement("insert into player values (?,?,?,? )");
		pstm.setInt(1, pid);
		pstm.setInt(4, jno);
		pstm.setString(2, fname);
		pstm.setString(3, lname);
		
		pstm.execute();
	}
	
	public static void deleteByIdFromPlayer(int id) throws Exception {
		
		Connection con = DBConnect.getConnection();
		
		PreparedStatement p = con.prepareStatement("delete from player where pid = ? ");
		p.setInt(1,id);
		p.execute();
		
	}
	
	public static List<PlayerBean> select(String[] arr) throws Exception {
		
		if(arr.length == 4)
			return selectAllPlayers();
		
		Connection con = DBConnect.getConnection();
		
		List<PlayerBean> l =new ArrayList<PlayerBean>();
		
		String tp ="";
		
		for(int i=0;i<arr.length-1;i++) {
			tp += "?,";
		}
		tp+="?";
		PreparedStatement pstm = con.prepareStatement("select "+tp+" from player ");
		int i=0;
		for(String s : arr) {
			pstm.setString(++i, s);
		}	
		
		ResultSet rs = pstm.executeQuery();
		
		
		while(rs.next()) {
			PlayerBean p =new PlayerBean();
			for(i=0;i<arr.length;i++){
				
				
				if(arr[i].equals("pid")) {
					p.setPid(rs.getInt(i+1));
				}else if(arr[i].equals("fname")) {
					p.setFname(rs.getString(i+1));
				}else if(arr[i].equals("lname")) {
					p.setLname(rs.getString(i+1));
				}else if(arr[i].equals("jno")) {
					p.setJerseyNumbaer(rs.getInt(i+1));
				}
				
				
			}
			l.add(p);
			
		}
		
		return l;
	}
	
}
