package com.sapient.day2;

public class PrimeNumber {

    public static boolean isPrime(double n) {

        double sqrt = Math.sqrt(n);

        if (n % 2 != 0) {
            for (int i = 3; i <= sqrt; i += 2) {
                if (n % i == 0)
                    return false;
            }
        } else
            return false;


        return true;
    }

}
