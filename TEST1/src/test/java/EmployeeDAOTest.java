import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


public class EmployeeDAOTest {

	EmployeeDAO eDAO = mock(EmployeeDAO.class);
	List<EmployeeBean> l;
	
	@Before
	public void init() {
		
		try {
		eDAO = new EmployeeDAO();
		l = eDAO.readData();
		}catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
		}
	}
	
	private EmployeeDAO mock(Class<EmployeeDAO> class1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Test
	public void getTotSalTest() {
		
		try {
			float sum = eDAO.getTotSal(l);
			
			Assert.assertEquals(657000f,sum,0.000001f );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void intGetCountTest() {
		
		try {
			
			int sum = eDAO.intGetCount(l,0);
			
			Assert.assertEquals(12,sum );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void getEmployeeTest() {
		
		try {
			
			EmployeeBean e = eDAO.getEmployee(0);
			
			boolean res = true;
			
			if(!e.getName().equalsIgnoreCase("Samson")) {
				res = false;
			}
			
			if(e.getSal() != 50000f)
				res = false;
			
			Assert.assertEquals(true,res );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}

