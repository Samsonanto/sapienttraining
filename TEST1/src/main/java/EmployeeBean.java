public class EmployeeBean {
	
	private float sal;
	private long id;
	private String name;
	
	public EmployeeBean(float sal, long id, String name) {
		super();
		this.sal = sal;
		this.id = id;
		this.name = name;
	}
	
	public EmployeeBean() {
		super();
	}
	
	
	
	public float getSal() {
		return sal;
	}

	public void setSal(float sal) {
		this.sal = sal;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		
		return "[ " + id + ", " + name + ", " + name + ", "+sal+" ]";  
	}
	
	@Override
	public boolean equals(Object o) {
		
		EmployeeBean e = (EmployeeBean)o;
		
		if(this.id != e.id)
			return false;
		if(this.sal != e.sal )
			return false;
		if(this.name != e.name)
			return false;
		
		return true;
		
	}
	
	@Override
	public int hashCode() {
		
		return this.toString().hashCode(); 
	}
	
}
