import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeDAOMockTest {

	@Mock
	EmployeeDAO eDAO;
	
	List<EmployeeBean> l;
	
	@Before
	public void init() {
		
		
		l =new ArrayList<EmployeeBean>();
		
		l.add(new EmployeeBean(40000f,1,"A"));
		l.add(new EmployeeBean(40000f,2,"B"));
		l.add(new EmployeeBean(40000f,3,"C"));
		l.add(new EmployeeBean(40000f,4,"D"));
		l.add(new EmployeeBean(40000f,5,"E"));
		l.add(new EmployeeBean(40000f,6,"F"));
		
		try {
			when(eDAO.readData()).thenReturn(l);
			when(eDAO.getTotSal(l)).thenCallRealMethod();
			when(eDAO.intGetCount(l,0)).thenCallRealMethod();
			when(eDAO.getEmployee(0)).thenCallRealMethod();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getTotSalTest() {
		
		try {
			float sum = eDAO.getTotSal(eDAO.readData());
			
			Assert.assertEquals(240000f,sum,0.000001f );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
	}
	@Test
	public void intGetCountTest() {
		
		try {
			
			int sum = eDAO.intGetCount(l,0);
			
			Assert.assertEquals(6,sum );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void getEmployeeTest() {
		
		try {
			
			EmployeeBean e = eDAO.getEmployee(0);
			
			boolean res = true;
			
			if(!e.getName().equalsIgnoreCase("A")) {
				res = false;
			}
			
			if(e.getSal() != 50000f)
				res = false;
			
			Assert.assertEquals(true,res );
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
