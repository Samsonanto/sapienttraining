package com.sapient.day2;

public class Matrix {

    private int[][]  matrix;

    Matrix(int size){
        matrix = new int[size][size];
    }
    Matrix(){
        matrix = new int[3][3];
    }

    Matrix(Matrix m){
        matrix = m.getMatrix();
    }

    int[][] getMatrix(){
        return matrix;
    }

    void setMatrix(){

        System.out.println("Enter " + matrix.length + " X " + matrix.length + " element");

        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix.length;j++){
                matrix[i][j] = Input.sc.nextInt();
            }
        }
    }

    int[][] add(int[][] m){

        int[][] res = new int[matrix.length][matrix.length];
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix.length;j++)
            {
                res[i][j] = m[i][j] + matrix[i][j];
            }
        }

        return res;
    }

    int[][] sub(int[][] m){

        int[][] res = new int[matrix.length][matrix.length];
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix.length;j++)
            {
                res[i][j] = m[i][j] - matrix[i][j];
            }
        }

        return res;
    }

    int[][] mul(int[][] m){
        int[][] res = new int[matrix.length][matrix.length];

        for(int i=0;i<m.length;i++){
            for(int k = 0; k<m.length;k++){
                for(int j = 0;j<m.length;j++){
                    res[i][j] = res[i][j] + matrix[i][j]*m[j][k];
                }
            }
        }

        return res;
    }
}
