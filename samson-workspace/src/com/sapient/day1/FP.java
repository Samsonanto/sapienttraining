package com.sapient.day1;

import java.util.logging.Logger;

interface math {
    int result(int a, int b);
}

public class FP {

    static Logger logger = Logger.getLogger(String.valueOf(FP.class));

    public static void main(String[] args) {

        math[] ob = new math[4];

        ob[0] = (a, b) -> (a + b);
        ob[1] = (a, b) -> (a - b);
        ob[2] = (a, b) -> (a * b);
        ob[3] = (a, b) -> (a / b);

        int x = Integer.parseInt(args[0]);
        int a = Integer.parseInt(args[1]);
        int b = Integer.parseInt(args[2]);


        logger.info("Result = " + ob[x - 1].result(a, b));


    }
}
