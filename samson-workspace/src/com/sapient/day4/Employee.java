package com.sapient.day4;

public class Employee {
	
	private String name;
	private String city;
	private int age;
	private long sal;
	public Employee(String name, String city, int age, long sal) {
		super();
		this.name = name;
		this.city = city;
		this.age = age;
		this.sal = sal;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public long getSal() {
		return sal;
	}
	public void setSal(long sal) {
		this.sal = sal;
	}
	
	
	public String toString() {
		return name+" : "+city+" : "+age+" : "+sal;
	}
	
	
}
