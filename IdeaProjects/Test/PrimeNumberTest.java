import com.sapient.day2.PrimeNumber;
import org.junit.Assert;
import org.junit.Test;

public class PrimeNumberTest {
    @Test
    public void isPrimeTest1() {
        boolean b = PrimeNumber.isPrime(12221);
        Assert.assertEquals(false, b);
    }

    @Test
    public void isPrimeTest2() {

        boolean b = PrimeNumber.isPrime(56468);
        Assert.assertEquals(false, b);

    }

    @Test
    public void isPrimeTest3() {

        boolean b = PrimeNumber.isPrime(5811111);
        Assert.assertEquals(false, b);

    }

    @Test
    public void isPrimeTest4() {

        boolean b = PrimeNumber.isPrime(89);
        Assert.assertEquals(true, b);

    }

    @Test
    public void isPrimeTest5() {

        boolean b = PrimeNumber.isPrime(1000000007);
        Assert.assertEquals(true, b);


    }

    @Test
    public void isPrimeTest6() {

        boolean b = PrimeNumber.isPrime(619);
        Assert.assertEquals(true, b);


    }

}
