package com.sapient.day5.Database;

public class MatchBean {

	private String mName;
	private int mid;
	public MatchBean(String mName, int mid) {
		super();
		this.mName = mName;
		this.mid = mid;
	}
	public MatchBean() {
		super();
	}
	public String getmName() {
		return mName;
	}
	public void setmName(String mName) {
		this.mName = mName;
	}
	public int getMid() {
		return mid;
	}
	public void setMid(int mid) {
		this.mid = mid;
	}
	
	
	public String toString() {
		
		return "Match Name : "+mName+" Match ID : "+mid;
		
	}
	
	
	
}
