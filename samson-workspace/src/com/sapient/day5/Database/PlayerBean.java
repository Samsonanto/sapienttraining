package com.sapient.day5.Database;

public class PlayerBean {

	
	private int pid;
	private int jerseyNumbaer;
	private String fname;
	private String lname;
	
	
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public int getJerseyNumbaer() {
		return jerseyNumbaer;
	}
	public void setJerseyNumbaer(int jerseyNumbaer) {
		this.jerseyNumbaer = jerseyNumbaer;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public PlayerBean(int pid, int jerseyNumbaer, String fname, String lname) {
		super();
		this.pid = pid;
		this.jerseyNumbaer = jerseyNumbaer;
		this.fname = fname;
		this.lname = lname;
	}
	public PlayerBean() {
		super();
	}
	
	
	public String toString() {
		
		return "PID : "+pid+" First Name : "+fname+" Last Name : "+lname;
		
	}
	
}
