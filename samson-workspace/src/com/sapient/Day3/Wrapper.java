package com.sapient.Day3;

public class Wrapper {
	
	private int a;
	
	
	public Wrapper() {
		this.a =0;
	}
	
	public Wrapper(int a) {
		// TODO Auto-generated constructor stub
		
		this.a = a;
		
	}
	
	public void setIntValue(int a) {
		this.a = a;
	}
	
	public int getIntValue()
	{
		return a;
	}
	
	public String toString() {
		return ""+a;
	}

}
