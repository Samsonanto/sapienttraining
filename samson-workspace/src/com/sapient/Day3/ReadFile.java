package com.sapient.Day3;

import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sampaul
 *
 */
public class ReadFile {
	
	
	public static void main(String[] args) throws Exception{
		
		Charset charset = Charset.forName("US-ASCII");
		
		Path path = FileSystems.getDefault().getPath("Test.csv");
		
		
		BufferedReader br = Files.newBufferedReader(path,charset);
		
		List<Integer> res = new ArrayList<Integer>();
		String line =null;
		do {
			
			line = br.readLine();
			
			if(line != null)
				res.add(line.split(",").length);
			
		}while(line != null);
		
		path = FileSystems.getDefault().getPath("Result.txt");
		Files.write(path,("").getBytes());
		
		
		for(Integer i : res) {
			String s = "" + i.intValue();
			Files.write(path,(s+"\n").getBytes(),StandardOpenOption.APPEND);
		}
		
		
		br.close();		
	}

}
