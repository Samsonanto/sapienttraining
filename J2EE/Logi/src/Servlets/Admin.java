package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Admin
 */
@WebServlet("/Admin")
public class Admin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Admin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("dclms");
		
		if(request.getAttribute("id") == null) {
			response.sendRedirect("/Logi/Login");
		}
		
		PrintWriter out = response.getWriter();
		
		out.print("<form action = \"Logout\" method=\"get\" >"+
				"<input type = \"submit\" value=\"Logout\" >" +
				"</form> ");
		
		out.print("<form action = \"InsertStudent\" method=\"get\" >"+
					"<input type = \"submit\" value=\"Insert\" >" +
					"</form> ");
		
		out.print("<form action=\" ListStudent \" method = \"get\">" + 
					"<input type=\"submit\" value=\" List Student\" ></form>");
		
		out.print("<form action=\" Logout \" method = \"get\">" + 
				"<input type=\"submit\" value=\" Log-out\" ></form>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.print("Here");
	}

}
