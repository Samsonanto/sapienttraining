package javaClass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PeopleDAO {
	
	
	
	public static boolean validate(int id,String pass) throws SQLException,ClassNotFoundException{
		
		Connection con = DBConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("select name from users where id = ? and password = ?");
		ps.setInt(1,id);
		ps.setString(2, pass);
		
		ResultSet rs =ps.executeQuery();
		
		if(rs.next()) {
			return true;
		}
		return false;
		
	}

	public static int getAccessRights(int id) throws SQLException,ClassNotFoundException {
		
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = con.prepareStatement("select type from users where id = ?");
		
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		
		rs.next();
		
		return rs.getInt(1);
		
	}
	
	public static Map<Integer,String> getListOfStudent() throws SQLException,ClassNotFoundException{
		
		Map<Integer,String> students =new HashMap<Integer,String>();
		
		Connection con = DBConnect.getConnection();
		PreparedStatement pstmt = con.prepareStatement("select id,name from users where type = 0");
		
		ResultSet rs = pstmt.executeQuery();
		
		while(rs.next()) {
			
			students.put(rs.getInt(1),rs.getString(2));
			
		}
		return students;
		
	}
	
	public static  Map<String,Float> getStudentMarks(int id) throws SQLException,ClassNotFoundException {
		
		Map<String,Float> res =new HashMap<String,Float>();
		
		Connection con = DBConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("select sub,marks from student_marks where id = ?");
		ps.setInt(1, id);

		System.out.println(id);
		ResultSet rs = ps.executeQuery();

		System.out.println("111");
		
		while(rs.next()) {
			res.put(rs.getString(1), rs.getFloat(2));
			System.out.println(rs.getString(1));
			
			
		}
		System.out.println("111");
		return res;
	}

	public static boolean addStudent(String name, String pass, String rno) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		Connection con = DBConnect.getConnection();
		PreparedStatement ps = con.prepareStatement("insert into users (id,name,type,password) values(?,?,0,?)");
		ps.setInt(1, Integer.parseInt(rno));
		ps.setString(2, name);
		ps.setString(3, pass);
		return ps.execute();
		
	}
	
	
}
