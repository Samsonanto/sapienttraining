package com.sapient.Day3;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

public class File2 {

	public static void main(String[] args) throws Exception {
		
		Path path = FileSystems.getDefault().getPath("Test1.txt");
		
		
		BufferedReader br = Files.newBufferedReader(path);
		
		String s = "bus";
		String repl = "1";
		ArrayList<String> str = new ArrayList<String>();
		
		
		
		String l = null;
		do {
			
			l = br.readLine();
			if(l != null) {
				
				str.add(l.toLowerCase().replaceAll(s, repl));
				int n = l.toLowerCase().indexOf(s);
				if(n!=-1)
					System.out.println(l);
				
			}
			
			
		}while(l != null);		
		
		Files.write(path,str.get(0).getBytes());
		Files.write(path, "\n".getBytes(), StandardOpenOption.APPEND);
		for(int i=1;i<str.size();i++ ) {
				Files.write(path, str.get(i).getBytes(), StandardOpenOption.APPEND);
				Files.write(path, "\n".getBytes(), StandardOpenOption.APPEND);	
		}
		
		
		
	}
	
}
