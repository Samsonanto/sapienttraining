package com.sapient.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Map;
import java.util.TreeMap;

import com.sapient.student.bean.StudentBean;

public class StudentDAO {
public Map<String, StudentBean> getAllStudents() throws Exception{
	Connection con = DBConnect.getConnection();
	PreparedStatement ps = con.prepareStatement("Select p.id,p.name,sd.rollno,sd.marks from person p,studentDetails sd where p.id = sd.id");
	Map<String,StudentBean> map = new TreeMap<String, StudentBean>();
	ResultSet rs = ps.executeQuery();
	ResultSetMetaData meta = rs.getMetaData();
	map.put("column_name", new StudentBean(meta.getColumnName(1), meta.getColumnName(2),meta.getColumnName(3), meta.getColumnName(4)));
	while(rs.next()) {
		String id = rs.getString(1);
		String name = rs.getString(2);
		String rollno = rs.getString(3);
		String marks = rs.getString(4);
		map.put(rollno, new StudentBean(id,name,rollno,marks));
	}
	return map;
}
}
