package javaClass;

public class PeopleBean {
	protected int id;
	protected String name;
	protected int mode;
	public PeopleBean(int id, String name, int mode) {
		super();
		this.id = id;
		this.name = name;
		this.mode = mode;
	}
	public PeopleBean() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMode() {
		return mode;
	}
	public void setMode(int mode) {
		this.mode = mode;
	}
	
}
