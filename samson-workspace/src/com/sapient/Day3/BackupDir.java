package com.sapient.Day3;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.sapient.util.Read;

public class BackupDir {

	static void copyDir(String src,String dst) throws Exception {
		
		Files.copy(Paths.get(src), Paths.get(dst));
		List<Path> pList =  Files.list(Paths.get(src)).collect(Collectors.toList());
		
		for(int i=0;i<pList.size();i++)
		{
			if(!Files.isDirectory(pList.get(i))) {
				Files.copy(pList.get(i), Paths.get(dst + "/" +  pList.get(i).getFileName().toString() ));
			}
			else {
				copyDir(pList.get(i).toAbsolutePath().toString(), dst +"/"+ pList.get(i).getFileName().toString());
			}
		}
		
		
	}
	
	
	public static void main(String[] args) throws Exception {
		
		
		System.out.println("Enter the src path of the directory to be copied ");
		String srcStr = Read.in.nextLine();
		
		System.out.println("Enter thr destination path");
		String dstStr = Read.in.nextLine();
	
		copyDir(srcStr,dstStr);
		
		
	}
	
	
}
