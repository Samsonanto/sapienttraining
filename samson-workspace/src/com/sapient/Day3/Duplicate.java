package com.sapient.Day3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.sapient.util.Read;

public class Duplicate {
	public static void main(String[] args) {
		
		System.out.println("Enter the size");
		
		int s = Read.in.nextInt();
		
		List<Integer> arr = new ArrayList<>();
		
		for(int i=0;i<s;i++) {
			
			arr.add(Read.in.nextInt());
			
		}
		
		List<Integer> l = arr.stream().distinct().collect(Collectors.toList());
		
		System.out.println(l);
	}
}
